# W-Boson-Lab-Course-Notebooks

Materials for the W boson lab course offered at III. physics institute A/B, RWTH Aachen University.

Open profile in RWTH Jupyter: [![](https://jupyter.pages.rwth-aachen.de/documentation/images/badge-launch-rwth-jupyter.svg)](https://jupyter.rwth-aachen.de/hub/spawn?profile=wblc)